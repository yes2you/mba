'use strict';

$(document).ready(function() {
  svg4everybody();
  $('.modules-item-title').click(function() {
    $(this).toggleClass('active');
    $(this).parent().find('.modules-item-content').stop().slideToggle();
  });
  $('.form-content').validate();
  $('input[type="tel"]').each(function() {
    var mask = $(this).attr('data-mask');
    $(this).mask(mask);
  });

  $('.js-scroll').click(function() {
    var target = $(this).attr('data-scroll');
    $('html, body').animate({
      scrollTop: $(target).offset().top
    });
  });
});
