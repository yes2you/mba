import gulp from 'gulp';
import sass from 'gulp-sass';
import sourcemaps from 'gulp-sourcemaps';
import ejs from 'gulp-ejs';
import gif from 'gulp-if';
import watch from 'gulp-watch';

import imagemin from 'gulp-imagemin';
import svgo from 'imagemin-svgo';
import sprite from 'gulp.spritesmith';
import svgSprite from 'gulp-svg-sprite';

import rm from 'gulp-rimraf';
import gutil from 'gulp-util';
import replace from 'gulp-replace';
import rename from 'gulp-rename';

import postcss from 'gulp-postcss';
import autoprefixer from 'autoprefixer';
import fixes from 'postcss-fixes';
import rucksack from 'rucksack-css';
import size from 'postcss-size';
import mq from 'css-mqpacker';
import stylefmt from 'stylefmt';

import browserSync from 'browser-sync';

const env = process.env.NODE_ENV || 'development';

const postcssConfig = [
  fixes({preset: 'fixes-only'}),
  autoprefixer({browsers: ['chrome 45', 'firefox 40', 'ie 9', 'safari 7', 'android 4']}),
  rucksack()
];

browserSync.init({
  server: './build/'
});

gulp.task('ejs', () => {
  gulp.src('./src/*.ejs')
    .pipe(ejs(null, {ext: '.html'}).on('error', gutil.log))
    .pipe(gulp.dest('./build'))
    .pipe(browserSync.stream());
});

gulp.task('scss', () => {
  gulp.src('./src/scss/main.scss')
    .pipe(gif(env === 'development', sourcemaps.init()))
    .pipe(sass({
      includePaths: [
        'node_modules/susy/sass',
        'node_modules'
      ]
    }).on('error', sass.logError))
    .pipe(postcss(postcssConfig))
    .pipe(gif(env === 'development', sourcemaps.write()))
    .pipe(gulp.dest('./build/css/'))
    .pipe(browserSync.stream());
});

gulp.task('css', () => {
  gulp.src('./src/css/**/*.css')
    .pipe(gulp.dest('./build/css/'))
    .pipe(browserSync.stream());
});

gulp.task('js', () => {
  gulp.src('./src/js/**/*.js')
    .pipe(gulp.dest('./build/js'))
    .pipe(browserSync.stream());
});

gulp.task('images', () => {
  gulp.src(['./src/img/**/*.{jpg,png}'])
    .pipe(imagemin())
    .pipe(gulp.dest('./build/img/'));
});

gulp.task('fonts', () => {
  gulp.src('./src/fonts/**/*.*')
    .pipe(gulp.dest('./build/fonts/'))
    .pipe(browserSync.stream());
});

gulp.task('assets', () => {
  gulp.src('./src/assets/**/*.*')
    .pipe(gulp.dest('./build/assets/'))
    .pipe(browserSync.stream());
});

gulp.task('sprite', () => {
  const spriteData =
    gulp.src('./src/img/sprite/*.png')
      .pipe(sprite({
        imgName: 'icons/sprite.png',
        cssName: '_sprite.scss',
        imgPath: '../img/icons/sprite.png'
      }));
  spriteData.img.pipe(gulp.dest('./build/img/'));
  spriteData.css.pipe(gulp.dest('./src/scss/base/'));
});

gulp.task('svg-sprite', () => {
  return gulp.src('./src/img/svg-sprite/*.svg')
    .pipe(imagemin([
      imagemin.svgo({plugins: []})
    ]))
    .pipe(svgSprite({
      mode: {
        symbol: {
          sprite: 'sprite.svg',
          render: {
            scss: {
              dest:'../../../../src/scss/base/_svg-sprite.scss',
              template: './src/scss/utils/helpers/_svg-template.scss'
            }
          }
        }
      }
    }))
    // .pipe(rename('sprite.svg'))
    .pipe(gulp.dest('./build/img/icons/'))
    .pipe(browserSync.stream());
});

gulp.task('clean', () => {
  rm('./build/');
});

gulp.task('watch', () => {
  watch('./src/**/*.ejs', () => {
    gulp.start('ejs');
  });
  watch('./src/scss/**/*.scss', () => {
    gulp.start('scss');
  });
  watch('./src/css/**/*.css', () => {
    gulp.start('css');
  });
  watch('./src/js/**/*.js', () => {
    gulp.start('js');
  });
  watch('./src/fonts/**/*.*', () => {
    gulp.start('fonts');
  });
  watch('./src/assets/**/*.*', () => {
    gulp.start('assets');
  });
  watch(['./src/img/**/*.{jpg,png}'], () => {
    gulp.start('images');
  });
  watch('./src/img/sprite/*.*', () => {
    gulp.start('sprite');
  });
  watch('./src/img/svg-sprite/*.svg', () => {
    gulp.start('svg-sprite');
  });
});

gulp.task('default', ['clean', 'ejs', 'scss', 'css', 'js', 'images', 'sprite', 'svg-sprite', 'fonts', 'assets', 'watch']);
